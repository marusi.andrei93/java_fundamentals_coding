
public class Main {
    public static void main(String[] args) {
        System.out.println("Hello Andrei!");
    }
}

//git init- este comnanda de initializare a unui proiect de git local
//culoarea rosu sunt proiecte neadaugate pe git
// verde fisiere bagate pe git
//albastru fiesiere adaugate pe git si modificate
// git add . este comanda de a adauga  fisierele noi/ modificari noi  pe git din proiectul curent(local)
// git commit -m "mesajul meu de commit(sa reflecte ce modificari s-au facut)"  salveaza o secventa de cod, iar mesajul din ghilimele specifica ce s-a facut
//este comanda care salveaza modificarile in git local(mesajul unui commit trebuie sa descrie modificarile unui commit)


// git remote add origin https://gitlab.com/marusi.andrei93/java_fundamentals_coding.git
// comanda cu care proiectul de pe git lab se conecteaza pe calculatorul nostru(masina locala)- face leg dintre git lab si calc nostru

//git push --set-upstream origin master  este comanda care trimite catre Git Lab ramura principala(master) a proiectului

//in git avem notiunea deb branch( ramura). Atunci cand avem un branch nou la primul push trebuir sa adaugam --set-upstream origin [nume branch]
//aceasta comanda se executa doar odata, apoi se poate executa doar comanda:  git push


// git checkout -b (nume branch) este comanda care creaza un nou branch(ramura)
//IMPORTANT!  Petru a putea schimba ramura inapoi pe master. toate modificarile aduse trebuie sa fie salvate intr-un comit

// git checkout (nume branch)   Schimba branch-ul cu ce branch doresti

//git pull este comanda de descarcare a moodificarilor din git lab in intelij


