package ex_afisare_numere_pare;

public class Main {
    public static void main(String[] args) {
        //Afisati toate numerele pare pana la 99 naturale

        for (int i = 0; i <= 99; i++) {
            if (i % 2 == 0) {
                System.out.println(i);
            }
        }
        System.out.println("varinta nr 2 este:");
        for (int i=0; i <99; i += 2){
            System.out.println(i);
        }
    }
}
